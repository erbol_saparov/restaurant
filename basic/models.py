from django.db import models
from smart_selects.db_fields import ChainedForeignKey


class Regions(models.Model):
    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'

    name = models.CharField(max_length=30, verbose_name='Какой регион:')

    def __str__(self):
        return self.name


class Categories(models.Model):
    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    name = models.CharField(max_length=30, verbose_name='Название:')
    region = models.ForeignKey(Regions, on_delete=models.SET_NULL, related_name='region', verbose_name='Регион',
                               null=True)
    image = models.ImageField(verbose_name='Фото категории', upload_to='media/')
    description = models.TextField(verbose_name='Описание категории')

    def __str__(self):
        return self.name


class Comment(models.Model):
    email = models.EmailField()
    text = models.TextField()

    def __str__(self):
        return self.email


class ProductPhotoes(models.Model):
    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фоты'

    image = models.ImageField(upload_to='media')


class Products(models.Model):
    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'

    name = models.CharField(max_length=30, verbose_name='Название:')
    region = models.ForeignKey(Regions, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='Регион')
    category = ChainedForeignKey(
        Categories,
        chained_field='region',
        chained_model_field='region',
        show_all=False,
        auto_choose=True,
        sort=True,
        null=True,
        blank=True,
        verbose_name='Категория'
    )
    image = models.ImageField(verbose_name='Фото:', upload_to='media', null=True)
    price = models.PositiveIntegerField('Стоимость:')

    def __str__(self):
        return self.name


class Cart(models.Model):
    cart_id = models.CharField(max_length=250, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.cart_id


class CartItem(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    total = models.IntegerField(default=0)

    def __str__(self):
        return self.product.name
