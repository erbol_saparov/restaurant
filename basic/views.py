import json

from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views import generic
from rest_framework.viewsets import ModelViewSet

from basic.models import *
from basic.serializers import ProductSerializer


def basic(request):
    cnt_products = Products.objects.all().count()
    data = {
        'cart_items': CartItem.objects.order_by('-created_at').all()[:3],
        'comments': Comment.objects.all(),
        'regions': Regions.objects.all(),
        'categories': Categories.objects.all(),
        'products_first': Products.objects.filter(id__lt=(cnt_products / 2)),
        'products_second': Products.objects.filter(id__gte=(cnt_products / 2))
    }
    return render(request, 'index.html', data)


def regionAPIView(request, reg_id):
    data = {
        'region': Regions.objects.get(id=reg_id),
        'area': Regions.objects.get(id=reg_id),
        # 'regions': Regions.objects.all(),
        'products': Products.objects.filter(region_id=reg_id),
    }
    return render(request, 'region.html', data)


class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = 'registration/signup.html'


def _cart_id(request):
    cart = request.session.session_key
    if not cart:
        cart = request.session.create()
    return cart


def add_cart(request, product_id):
    product = Products.objects.get(id=product_id)
    try:
        cart = Cart.objects.get(cart_id=_cart_id(request))  # get the cart using the cart id present in the sessie
    except Cart.DoesNotExist:
        cart = Cart.objects.create(cart_id=_cart_id(request))
        cart.save()

    try:
        cart_item = CartItem.objects.get(product=product, cart=cart)
        cart_item.quantity += 1
        cart_item.total = cart_item.quantity * cart_item.product.price
        cart_item.save()
    except CartItem.DoesNotExist:
        cart_item = CartItem.objects.create(
            product=product,
            quantity=1,
            total=product.price,
            cart=cart,)
        cart_item.save()

    return redirect('basic')


def cart_view(request):
    cnt_products = Products.objects.all().count()
    summa = 0
    carts = CartItem.objects.all()
    for i in carts:
        summa += i.total
    context = {
        'cart_items': CartItem.objects.order_by('-created_at').all(),
        'regions': Regions.objects.all(),
        'categories': Categories.objects.all(),
        'products_first': Products.objects.filter(id__lt=(cnt_products / 2)),
        'products_second': Products.objects.filter(id__gte=(cnt_products / 2)),
        'all_total': summa
    }

    return render(request, 'cart.html', context)


def comment(request):
    if request.method == 'POST':
        email = request.POST['email']
        text = request.POST['text']
    review = Comment.objects.create(email=email, text=text)
    review.save()

    return redirect('basic')


from rest_framework.decorators import api_view
from rest_framework.response import Response

#api
class ProductViewSet(ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer


class RegionViewSet(ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
