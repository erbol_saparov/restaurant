from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path, include

from basic import views
from basic.views import SignUpView, ProductViewSet

urlpatterns = [
    path('', views.basic, name='basic'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('view_cart/', views.cart_view, name='view_cart'),
    path('add_cart/<int:product_id>/', views.add_cart, name='add_cart'),
    path('comment/', views.comment, name='comment'),
    # path('delete/<int:id>/', views.remove_from_cart, name='remove_cart'),
    path('accounts/login/', LoginView.as_view(), name='login'),
    path('accounts/logout/', LogoutView.as_view(), name='logout'),
    path('region/<int:reg_id>', views.regionAPIView, name='region'),

    #api
    path('api/product/', ProductViewSet.as_view({'get': 'list', 'post': 'create'}), name='api_product')
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)