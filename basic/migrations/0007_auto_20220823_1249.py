# Generated by Django 3.2 on 2022-08-23 06:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('basic', '0006_auto_20220822_1819'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductPhotoes',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='media')),
            ],
            options={
                'verbose_name': 'Фото',
                'verbose_name_plural': 'Фоты',
            },
        ),
        migrations.AlterField(
            model_name='products',
            name='image',
            field=models.ImageField(upload_to='media', verbose_name='Фото:'),
        ),
    ]
