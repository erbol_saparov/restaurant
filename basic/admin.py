from django.contrib import admin

from basic.models import *


class RegionsInline(admin.TabularInline):
    model = Categories
    fields = ['name']
    extra = 1


class RegionAdmin(admin.ModelAdmin):
    inlines = RegionsInline,


class CategoryInline(admin.TabularInline):
    model = Products
    exclude = ['region', 'image']
    extra = 1


class CategoryAdmin(admin.ModelAdmin):
    inlines = CategoryInline,
    list_display = ['name', ]


admin.site.register(Regions, RegionAdmin)
admin.site.register(Categories, CategoryAdmin)
admin.site.register(Products)
admin.site.register(Cart)
admin.site.register(CartItem)
admin.site.register(Comment)